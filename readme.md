

```
CGO_ENABLED=0 GOOS=darwin go build -a -installsuffix cgo -o bin/inlets-darwin
```


```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/inlets
CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6 go build -a -installsuffix cgo -o bin/inlets-armhf
CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -a -installsuffix cgo -o bin/inlets-arm64
```
